/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaleilao_v2.controller;

import java.util.List;
import sistemaleilao_v2.model.Cliente;
import sistemaleilao_v2.model.Leilao;

/**
 *
 * @author Vitor
 */
public class ControllerLeilao {
    private Leilao leilao;

    public ControllerLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    /**
     * @return the leilao
     */
    public Leilao getLeilao() {
        return leilao;
    }

    /**
     * @param leilao the leilao to set
     */
    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }
    
   public void addNovoClienteConectado(Cliente cliente){
       this.leilao.getClientesConectados().add(cliente);
   }
   
   public void setAtualClienteVencedor(Cliente cliente){
       this.leilao.setClienteLanceVencedor(cliente);
   }
   
   public void setValorLanceVencedor(double valor){
       this.leilao.setValorLanceVencedor(valor);
   }
   
   public void setQuantidadeLancesrealizados(int quantidade){
       this.leilao.setQuantidadeTotalDeLancesRealizados(quantidade);
   }
   
   public List<Cliente> getClientesConectados(){
       return this.leilao.getClientesConectados();
   }
}
