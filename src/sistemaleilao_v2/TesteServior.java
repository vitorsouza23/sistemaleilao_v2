/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaleilao_v2;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import sistemaleilao_v2.cliente.ClienteLeilao;
import sistemaleilao_v2.controller.ControllerLeilao;
import sistemaleilao_v2.model.Cliente;
import sistemaleilao_v2.model.Leilao;
import sistemaleilao_v2.model.Produto;
import sistemaleilao_v2.servidor.ServidorLeilao;

/**
 *
 * @author Vitor
 */
public class TesteServior {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Produto produto = new Produto("p1", "c1");
        Leilao leilao = new Leilao(produto, 0, 0, 0, new ArrayList<>(), null);
        ControllerLeilao controllerLeilao = new ControllerLeilao(leilao);
        ServidorLeilao servidor = new ServidorLeilao(1234, 5, controllerLeilao);
        try {
            servidor.runServidorLeilao();
        } catch (SocketException ex) {
            Logger.getLogger(TesteServior.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        
    }
    
}
