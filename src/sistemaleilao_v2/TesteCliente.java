/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaleilao_v2;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sistemaleilao_v2.cliente.ClienteLeilao;
import sistemaleilao_v2.model.Cliente;

/**
 *
 * @author Vitor
 */
public class TesteCliente {
    public static void main(String[] args) {
         Cliente cliente = new Cliente("c1", 0.0, 0.0);
        ClienteLeilao clienteLeilao = new ClienteLeilao(cliente, "192.168.0.146", 1234);
        try {
            clienteLeilao.conectarAoServidorDeLeilao();
        } catch (IOException ex) {
            Logger.getLogger(TesteServior.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
