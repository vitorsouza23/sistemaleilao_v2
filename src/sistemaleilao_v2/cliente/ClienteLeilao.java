/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaleilao_v2.cliente;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.SocketException;
import sistemaleilao_v2.model.Cliente;

/**
 *
 * @author Vitor
 */
public class ClienteLeilao {
    private final Cliente cliente;
    private final String endereco;
    private final int porta;

    public ClienteLeilao(Cliente cliente, String endereco, int porta) {
        this.cliente = cliente;
        this.endereco = endereco;
        this.porta = porta;
    }
    
    public void conectarAoServidorDeLeilao() throws SocketException, IOException{
        DatagramSocket clienteLielao = new DatagramSocket();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(this.cliente);
        byte[] dados = baos.toByteArray();
        DatagramPacket pacoteEnvio = new DatagramPacket(dados,dados.length, InetAddress.getByName(this.endereco), this.porta);
        clienteLielao.send(pacoteEnvio);
        System.out.println("Cliente " + this.cliente.getNome() + " eviou a mensagem!");
        
        byte[] dadosRecebidos = new byte[1024];
        DatagramPacket pacoteRecebido = new DatagramPacket(dadosRecebidos, dadosRecebidos.length);
        clienteLielao.receive(pacoteRecebido);
        String resposta = new String(pacoteRecebido.getData());
        System.out.println("Resposta do Servidor: " + resposta);
        
    }
    
}
