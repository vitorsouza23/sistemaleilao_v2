/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaleilao_v2.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Vitor
 */
public class Leilao implements Serializable{
    private Produto produto;
    private double precoInicial;
    private int quantidadeTotalDeLancesRealizados;
    private double valorLanceVencedor;
    private List<Cliente> clientesConectados;
    private Cliente clienteLanceVencedor;
    

    public Leilao(Produto produto, double precoInicial, int quantidadeTotalDeLancesRealizados, double valorLanceVendedor, List<Cliente> clientesConectados, Cliente clienteLanceVencedor) {
        this.produto = produto;
        this.precoInicial = precoInicial;
        this.quantidadeTotalDeLancesRealizados = quantidadeTotalDeLancesRealizados;
        this.valorLanceVencedor = valorLanceVendedor;
        this.clientesConectados = clientesConectados;
        this.clienteLanceVencedor = clienteLanceVencedor;
    }

    /**
     * @return the produto
     */
    public Produto getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    /**
     * @return the precoInicial
     */
    public double getPrecoInicial() {
        return precoInicial;
    }

    /**
     * @param precoInicial the precoInicial to set
     */
    public void setPrecoInicial(double precoInicial) {
        this.precoInicial = precoInicial;
    }

    /**
     * @return the quantidadeTotalDeLancesRealizados
     */
    public int getQuantidadeTotalDeLancesRealizados() {
        return quantidadeTotalDeLancesRealizados;
    }

    /**
     * @param quantidadeTotalDeLancesRealizados the quantidadeTotalDeLancesRealizados to set
     */
    public void setQuantidadeTotalDeLancesRealizados(int quantidadeTotalDeLancesRealizados) {
        this.quantidadeTotalDeLancesRealizados = quantidadeTotalDeLancesRealizados;
    }

    /**
     * @return the valorLanceVencedor
     */
    public double getValorLanceVencedor() {
        return valorLanceVencedor;
    }

    /**
     * @param valorLanceVencedor the valorLanceVencedor to set
     */
    public void setValorLanceVencedor(double valorLanceVencedor) {
        this.valorLanceVencedor = valorLanceVencedor;
    }

    /**
     * @return the clientesConectados
     */
    public List<Cliente> getClientesConectados() {
        return clientesConectados;
    }

    /**
     * @param clientesConectados the clientesConectados to set
     */
    public void setClientesConectados(List<Cliente> clientesConectados) {
        this.clientesConectados = clientesConectados;
    }

    /**
     * @return the clienteLanceVencedor
     */
    public Cliente getClienteLanceVencedor() {
        return clienteLanceVencedor;
    }

    /**
     * @param clienteLanceVencedor the clienteLanceVencedor to set
     */
    public void setClienteLanceVencedor(Cliente clienteLanceVencedor) {
        this.clienteLanceVencedor = clienteLanceVencedor;
    }
    
}
