/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaleilao_v2.model;

import java.io.Serializable;

/**
 *
 * @author Vitor
 */
public class Cliente implements Serializable{
    private String nome;
    private double valorLanceAtual;
    private double valorNovoLance;

    public Cliente(String nome, double valorLanceAtual, double valorNovoLance) {
        this.nome = nome;
        this.valorLanceAtual = valorLanceAtual;
        this.valorNovoLance = valorNovoLance;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the valorLanceAtual
     */
    public double getValorLanceAtual() {
        return valorLanceAtual;
    }

    /**
     * @param valorLanceAtual the valorLanceAtual to set
     */
    public void setValorLanceAtual(double valorLanceAtual) {
        this.valorLanceAtual = valorLanceAtual;
    }

    /**
     * @return the valorNovoLance
     */
    public double getValorNovoLance() {
        return valorNovoLance;
    }

    /**
     * @param valorNovoLance the valorNovoLance to set
     */
    public void setValorNovoLance(double valorNovoLance) {
        this.valorNovoLance = valorNovoLance;
    }
    
    
}
