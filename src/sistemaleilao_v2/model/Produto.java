/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaleilao_v2.model;

import java.io.Serializable;

/**
 *
 * @author Vitor
 */
public class Produto implements Serializable{
    private String nome;
    private String caracteristivas;

    public Produto() {
    }

    public Produto(String nome, String caracteristivas) {
        this.nome = nome;
        this.caracteristivas = caracteristivas;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the caracteristivas
     */
    public String getCaracteristivas() {
        return caracteristivas;
    }

    /**
     * @param caracteristivas the caracteristivas to set
     */
    public void setCaracteristivas(String caracteristivas) {
        this.caracteristivas = caracteristivas;
    }
    
    
}
