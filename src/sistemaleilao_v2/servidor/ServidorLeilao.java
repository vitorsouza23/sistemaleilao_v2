/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaleilao_v2.servidor;

import java.net.DatagramSocket;
import java.net.SocketException;
import sistemaleilao_v2.controller.ControllerLeilao;
import sistemaleilao_v2.model.Leilao;

/**
 *
 * @author Vitor
 */
public class ServidorLeilao {

    private final int portaServidor;
    private final int totalComunicacoesSimultaneas;
    private final ControllerLeilao controllerLeilao;

    public ServidorLeilao(int portaServidor, int totalComunicacoesSimultaneas, ControllerLeilao controllerLeilao) {
        this.portaServidor = portaServidor;
        this.totalComunicacoesSimultaneas = totalComunicacoesSimultaneas;
        this.controllerLeilao = controllerLeilao;
    }

    
    
    public void runServidorLeilao() throws SocketException {
        DatagramSocket serverSocket = new DatagramSocket(this.portaServidor);
        while(true){
            if(this.controllerLeilao.getClientesConectados().size() <= this.totalComunicacoesSimultaneas){
               new ThreadConexoes(serverSocket, this.controllerLeilao).run();
            }
        }
    }

    
}
