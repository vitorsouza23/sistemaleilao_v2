/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaleilao_v2.servidor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
import sistemaleilao_v2.controller.ControllerLeilao;
import sistemaleilao_v2.model.Cliente;
import sistemaleilao_v2.model.Leilao;

/**
 *
 * @author Vitor
 */
public class ThreadConexoes extends Thread{
    private Cliente novoCliente;
    private final DatagramSocket serverSoket;
    private byte[] dadosRecebidos;
    private final ControllerLeilao controllerLeilao;

    public ThreadConexoes(DatagramSocket serverSoket, ControllerLeilao controllerLeilao) {
        this.serverSoket = serverSoket;
        this.controllerLeilao = controllerLeilao;
    }
    
    @Override
    public void run(){
        this.dadosRecebidos = new byte[1024];
        while(true){
            DatagramPacket pacoteEntrada = new DatagramPacket(this.dadosRecebidos, this.dadosRecebidos.length);
            System.out.println("Servidor inicilaizado...");
            try {
                this.serverSoket.receive(pacoteEntrada);
                byte[] dados = pacoteEntrada.getData();
                ByteArrayInputStream bais = new ByteArrayInputStream(dados);
                ObjectInputStream ois = new ObjectInputStream(bais);
                this.novoCliente = (Cliente) ois.readObject();
                
                this.controllerLeilao.addNovoClienteConectado(novoCliente);
                
                String resposta = "Cliente "+ this.novoCliente.getNome() +" conectado!";
                byte[] respostaByte = resposta.getBytes();
                DatagramPacket pacoteSaida = new DatagramPacket(respostaByte, respostaByte.length, pacoteEntrada.getAddress(), pacoteEntrada.getPort());
                this.serverSoket.send(pacoteSaida);
            } catch (IOException | ClassNotFoundException ex) {
                Logger.getLogger(ThreadConexoes.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }
    
    
}
